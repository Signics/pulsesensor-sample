package com.hello_things.pulse;

import android.os.Handler;

import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.PeripheralManagerService;

import java.io.IOException;

/**
 * Created by owner on 2017/02/20.
 */

public class FullColorLed {

    private Gpio mBlue;
    private Gpio mRed;
    private Gpio mGreen;
    private Handler mHandler = new Handler();

    public static final int Blue = 0;
    public static final int Yello = 1;
    public static final int Red = 2;

    FullColorLed(){
        PeripheralManagerService service = new PeripheralManagerService();

        try {
            mBlue = service.openGpio("BCM16");
            configureOut(mBlue);
            mRed = service.openGpio("BCM20");
            configureOut(mRed);
            mGreen = service.openGpio("BCM21");
            configureOut(mGreen);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void configureOut(Gpio gpio) throws IOException {
        gpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
        gpio.setActiveType(Gpio.ACTIVE_HIGH);
    }

    public void setColor(int color) throws IOException {
        boolean red, green, blue ;
        switch (color) {
            case Blue:
                red = false;
                green = false;
                blue = true;
                break;
            case Yello:
                red = true;
                green = true;
                blue = false;
                break;
            case Red:
                red = true;
                green = false;
                blue = false;
                break;
            default:
                red = false;
                green = false;
                blue = true;
                break;
        }
        setColor(red, green, blue);
    }

    public void setColor(final boolean red, final boolean green, final boolean blue) throws IOException {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mRed.setValue(red);
                    mGreen.setValue(green);
                    mBlue.setValue(blue);

                    Thread.sleep(300);

                    mRed.setValue(false);
                    mGreen.setValue(false);
                    mBlue.setValue(false);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
