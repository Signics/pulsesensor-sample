package com.hello_things.pulse;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.hello_things.pulse.PulseSensor;

import android.app.Activity;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * メインアクティビティ
 */
public class MainActivity extends Activity implements PulseSensor.Listener {

    private LineChart mChart;
    private LineChart mBmpChart;
    private PulseSensor mSensorEngine;
    private FullColorLed mColorLed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSensorEngine = new PulseSensor(100);
        mSensorEngine.setListener(this);
        mColorLed = new FullColorLed();
        mChart = (LineChart) findViewById(R.id.chart);
        mBmpChart = (LineChart) findViewById(R.id.bmp_chart);
        initChart();
        initBmpChart();
        startMonitoring();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSensorEngine.isRunning()) {
            stopMonitoring();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSensorEngine.destroy();
    }

    private void initBmpChart() {
        // no description text
        mBmpChart.setDescription("");
        mBmpChart.setNoDataTextDescription("You need to provide data for the chart.");

        // enable touch gestures
        mBmpChart.setTouchEnabled(true);

        // enable scaling and dragging
        mBmpChart.setDragEnabled(true);
        mBmpChart.setScaleEnabled(true);
        mBmpChart.setDrawGridBackground(false);

        // if disabled, scaling can be done on x- and y-axis separately
        mBmpChart.setPinchZoom(true);

        // set an alternative background color
        mBmpChart.setBackgroundColor(Color.WHITE);

        LineData data = new LineData();
        data.setValueTextColor(Color.rgb(0x26, 0x32, 0x38));

        // add empty data
        mBmpChart.setData(data);

        //  ラインの凡例の設定
        Legend l = mBmpChart.getLegend();
        l.setForm(Legend.LegendForm.LINE);
        l.setTextColor(Color.rgb(0x26, 0x32, 0x38));

        XAxis xl = mBmpChart.getXAxis();
        xl.setTextColor(Color.rgb(0x26, 0x32, 0x38));
        xl.setLabelsToSkip(9);

        YAxis leftAxis = mBmpChart.getAxisLeft();
        leftAxis.setTextColor(Color.rgb(0x26, 0x32, 0x38));
        leftAxis.setAxisMaxValue(130.0f);
        leftAxis.setAxisMinValue(30.0f);
        leftAxis.setStartAtZero(false);
        leftAxis.setDrawGridLines(true);

        YAxis rightAxis = mBmpChart.getAxisRight();
        rightAxis.setEnabled(false);
    }

    private void initChart() {
        // no description text
        mChart.setDescription("");
        mChart.setNoDataTextDescription("You need to provide data for the chart.");

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);

        // set an alternative background color
        mChart.setBackgroundColor(Color.WHITE);

        LineData data = new LineData();
        data.setValueTextColor(Color.rgb(0x26, 0x32, 0x38));

        // add empty data
        mChart.setData(data);

        //  ラインの凡例の設定
        Legend l = mChart.getLegend();
        l.setForm(Legend.LegendForm.LINE);
        l.setTextColor(Color.rgb(0x26, 0x32, 0x38));

        XAxis xl = mChart.getXAxis();
        xl.setTextColor(Color.rgb(0x26, 0x32, 0x38));
        xl.setLabelsToSkip(9);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setTextColor(Color.rgb(0x26, 0x32, 0x38));
        leftAxis.setAxisMaxValue(3.0f);
        leftAxis.setAxisMinValue(0.0f);
        leftAxis.setStartAtZero(false);
        leftAxis.setDrawGridLines(true);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);
    }

    private void startMonitoring() {
        mSensorEngine.start();
    }

    private void stopMonitoring() {
        mSensorEngine.stop();
    }

    @Override
    public void onSensorVoltage(Date date, double value) {
        LineData data = mChart.getData();
        if (data == null) {
            return;

        }

        LineDataSet set = data.getDataSetByIndex(0);
        if (set == null) {
            set = new LineDataSet(null, "心拍センサー[V]");
            set.setDrawCircles(false);
            set.setColor(Color.rgb(0x00, 0x96, 0x88));
            set.setDrawValues(false);
            data.addDataSet(set);
        }

        //  追加描画するデータを追加
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        data.addXValue(format.format(date));
        data.addEntry(new Entry((float) value, set.getEntryCount()), 0);

        //  データを追加したら必ずよばないといけない
        mChart.notifyDataSetChanged();

        mChart.setVisibleXRangeMaximum(60);

        mChart.moveViewToX(data.getXValCount() - 61);   //  移動する
    }

    @Override
    public void onBPM(Date date, int value) {
        LineData data = mBmpChart.getData();
        if (data == null) {
            return;

        }

        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText("心拍数：" + value);
        LineDataSet set = data.getDataSetByIndex(0);
        if (set == null) {
            set = new LineDataSet(null, "BMP");
            set.setDrawCircles(false);
            set.setColor(Color.rgb(0xE9, 0x1E, 0x63));
            set.setDrawValues(false);
            data.addDataSet(set);
        }

        //  追加描画するデータを追加
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        data.addXValue(format.format(date));
        data.addEntry(new Entry((float) value, set.getEntryCount()), 0);

        //  データを追加したら必ずよばないといけない
        mBmpChart.notifyDataSetChanged();

        mBmpChart.setVisibleXRangeMaximum(60);

        mBmpChart.moveViewToX(data.getXValCount() - 61);   //  移動する
    }

    @Override
    public void onHeartBeat(int value) {
        int color = FullColorLed.Blue;
        try {
            if(value < 50) {
                color = FullColorLed.Blue;
            } if (value >= 50 && value < 95) {
                color = FullColorLed.Yello;
            } if (value >= 95) {
                color = FullColorLed.Red;
            }
            mColorLed.setColor(color);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
