package com.hello_things.pulse;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import com.google.android.things.pio.PeripheralManagerService;
import com.google.android.things.pio.SpiDevice;

import java.io.IOException;
import java.util.Date;


public class PulseSensor {

    private static final String SPI_DEVICE_NAME = "SPI0.0";
    private SpiDevice mDeviceSpi;
    private Handler mHandler = new Handler();
    private final Handler mUiHandler;
    private Listener mListener = null;
    private boolean mRunning = false;
    private final int mInterval;
    private final HandlerThread mThread;

    private int mPulseCount = 0;
    private double mPrevPulse = 0;
    private int mIndex = 0;

    private int mBMP = 0;

    PulseSensor (int interval) {
        mInterval = interval;
        mUiHandler = new Handler();
        mThread = new HandlerThread("PulseSensor");
        mThread.start();
        mHandler = new Handler(mThread.getLooper());

        PeripheralManagerService service = new PeripheralManagerService();

        try {
            mDeviceSpi = service.openSpiDevice(SPI_DEVICE_NAME);

            mDeviceSpi.setMode(SpiDevice.MODE0);
            mDeviceSpi.setFrequency(120000);  //1.2MHz
            mDeviceSpi.setBitsPerWord(8);
            mDeviceSpi.setBitJustification(false);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start () {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            throw new RuntimeException("このメソッドはUIスレッドから呼び出してください。");
        } else if (mRunning) {
            return;
        }
        mRunning = true;
        mHandler.post(new Runnable() {

            @Override
            public void run() {
                if (!mRunning) {
                    return;
                }
                if (mDeviceSpi == null) {
                    return;
                }
                try {
                    byte [] request = new byte[]{ 0x78, 0x00};
                    byte [] response = new byte[2];

                    mDeviceSpi.transfer(request, response, response.length);
                    final double ad = (((response[0] & 0x03) << 8) + response[1]) * 3.3 / 1023;

                    final Date currentDate = new Date();

                    if(mPrevPulse < 1.5 && ad >= 1.5) {
                        mPulseCount++;
                        if (mListener != null) {
                            mListener.onHeartBeat(mBMP);
                        }
                    }

                    mPrevPulse = ad;

                    Log.d("debug", "AD = " + ad + "[V]");
                    mUiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (mListener != null) {
                                    mListener.onSensorVoltage(currentDate , ad);
                                }
                            }
                        });
                    if(mIndex == 200) {
                        Log.d("debug", "pulse = " + mPulseCount * 3);
                        mUiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (mListener != null) {
                                    mBMP = mPulseCount * 3;
                                    mListener.onBPM(currentDate, mBMP);
                                }
                                mPulseCount = 0;
                                mPrevPulse = 0.0;
                                mIndex = 0;
                            }
                        });
                    }
                } catch (IOException e) {

                }

                mIndex++;
                mHandler.postDelayed(this, mInterval);
            }
        });
    }

    void stop() {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            throw new RuntimeException("このメソッドはUIスレッドから呼び出してください。");
        }
        if (mRunning) {
            mRunning = false;
        }
    }

    void destroy() {
        if (mRunning) {
            return;
        }
        mThread.quit();
    }

    boolean isRunning() { return mRunning; }

    void setListener(Listener l) {
        mListener = l;
    }

    public interface Listener {
        void onSensorVoltage(Date date, double value);
        void onBPM(Date date, int value);
        void onHeartBeat(int value);
    }
}
